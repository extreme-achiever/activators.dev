+++
author = "Khang Tran"
categories = ["2FAs"]
date = 2020-01-15T00:00:00Z
description = ""
image = "/images/so-how-does-it-work.png"
title = "How Private 2FAs feature works !"
type = "post"

+++
When we first launched [PrivaKeeper](https://privakeeper.com/ "Launch PrivaKeeper") to our communities, the feedback were awesome. We are so grateful that the product has their (and maybe yours) praises about the clean, organized and easy-to-use looking.

In case many of our users don't get familiar with Private 2FAs feature, I have a simple explanation here in this blog.

**So, how does it work?**

The Private 2FAs feature is technically a Google Authenticator, but in a web version and only supports Time-based One Time Password.

Google Authenticator does generate the Time-based One Time Password (TOTP) expired in 30 seconds. After a 30-seconds period, it will re-generate new one, based on the encryptions of your secret code.

You can learn about Google Authenticator [here](https://support.google.com/accounts/answer/1066447?co=GENIE.Platform%3DAndroid&hl=en "Install Google Authenticator")

**A few notes**

Therefore, **Private 2FAs** feature requires only your **Secret Code** to work. Is it simple, right?

Here is the form you need to fill in.

![](https://blog.privakeeper.com/images/linked_in_step3.png)

After saving your **Secret Code**, you will have the generated OTP that is valid to perform authentication in the website generating the secret code (some websites may call it **Secret Key, Secret Token,** etc).

**Other input field explanation**

The **Title:** you may want to name what-so-ever to the 2FA entity

The **Account**: you may want to type username/email/id to remind the credential which matches the **Secret Code**

The **Website Url:** to remind you the website, and you can click to launch in new tab

The **Folder:** if you want to group the 2FA entity

**Conclusion**

So that's it. The Private 2FAs feature was built for the ease of use. If you still find it confuse or may want to ask more about us, please drop an email to [hello@privakeeper.com](mailto:hello@privakeeper.com).

To continue to explore, you may want to see the [**Private 2FAs Use Cases**](https://blog.privakeeper.com/categories/2fa-usecase/ "Use Cases of Private 2FAs feature")



Besides, you can try other Dapps:

PrivaKeeper - PRIVATE STORAGE THAT RESPECTS YOUR PRIVACY
https://privakeeper.com/

BlockResume - A modern resume generator which respects your privacy
https://blockresume.me/

Calorie - PRIVATE CALORIES COUNTER WEB APP
https://calorie.fit/landing

Finisher - PRIVATE ACTIVITY TRACKER APP
https://finisher.pro/landing

Book Billionaire
https://bookbillionaire.com/

Great Quotes - Great Quotes of all time
https://greatquotes.life/

Embolden It - A decentralized way to highlight words on website.
https://emboldenit.com/